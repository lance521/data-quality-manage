package com.sf.gis.dataqualitymanage.dataqualitymanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataQualityManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataQualityManageApplication.class, args);
    }

}
