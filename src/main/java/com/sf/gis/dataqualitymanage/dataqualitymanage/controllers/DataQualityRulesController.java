/**
 * Copyright (C), 2015-2022
 * FileName: DataQualityRulesController
 * Author:   caiguofang
 * Date:     2022/3/8 11:31
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.dataqualitymanage.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 数据质量规则管理, 增删改查
 * @ClassName DataQualityRulesController
 * @author Administrator
 * @create 2022/3/8 11:31
 * @since 1.0.0
 */

@RestController
@RequestMapping("dataqualityrules")
public class DataQualityRulesController {


}