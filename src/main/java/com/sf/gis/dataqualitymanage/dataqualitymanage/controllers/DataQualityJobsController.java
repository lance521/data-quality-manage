/**
 * Copyright (C), 2015-2022
 * FileName: DataQualityJobsController
 * Author:   caiguofang
 * Date:     2022/3/8 11:35
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.dataqualitymanage.controllers;

/**
 *  数据质量任务管理 -- 需要进行远程调用
 * @ClassName DataQualityJobsController
 * @author Administrator
 * @create 2022/3/8 11:35
 * @since 1.0.0
 */
public class DataQualityJobsController {

}